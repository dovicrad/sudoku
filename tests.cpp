//
// Created by radek on 15.01.2023.
//
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "BoardFactory.hpp"

TEST_CASE("Board test","[boardTest]") {
    auto* factory = new BoardFactory();
    Board* board = factory->create();
    SECTION("Factory creates non empty board ") {
        REQUIRE_FALSE(board == nullptr);
        bool empty = true;
        for(size_t x = 0; x < 8; x++){
            for(size_t y = 0; y < 8; y++){
                if(board->getTile(x,y) != nullptr){
                    empty = false;
                    break;
                }
            }
        }
        REQUIRE_FALSE(empty);
    }

    board = new Board();
    SECTION("Board correctly flags incorrectly placed tiles") {
        //tile outside of board
        CHECK_THROWS(board->setTile(-6,-6, nullptr));
        //tile value other than 1-9
        CHECK_THROWS(board->setTile(1,1,(size_t)0));
    }

    board = new Board();
    SECTION("Board correctly flags tiles placed against sudoku rules") {
        board->setTile(0,0,1);
        REQUIRE(board->isProhibited(1,1,1));
        REQUIRE(board->isProhibited(0,8,1));
        REQUIRE(board->isProhibited(8,0,1));
        REQUIRE_FALSE(board->isProhibited(3,3,1));
    }
}
