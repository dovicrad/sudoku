//
// Created by radek on 10.12.2022.
//

#ifndef SUDOKU_USERINTERFACE_HPP
#define SUDOKU_USERINTERFACE_HPP


#include <iostream>
#include "Board.hpp"


const static char letters[] = {'a','b','c','d', 'e', 'f','g','h','i'};

class UserInterface {
    Board* board;
    static void closeGame();

    /*
     * takes string and makes a pair of x and y
     *
     * numbers -> x
     * letters -> y
     *
     * always takes the last inputted value
     *
     * param: user input
     * return: a pair of x and y coordinate
     * */
    static std::pair<size_t,size_t> translateCoords(const std::string& coordsString);

    /*
     * takes string and makes size_t single digit
     *
     * always takes last inputted value
     *
     * param: user input
     * return: size_t that contains value
     * */
    static size_t translateValue(const std::string& valueString);

    /*
     * asks user for coords and value
     *
     * return: tuple:
     * 0 -> x
     * 1 -> y
     * 2 -> value
     * */
    static std::tuple<size_t, size_t, size_t> getMove();

    /*
     * checks if input contains q
     *
     * param: user input
     */
    static bool checkForQ(const std::string&);

    public:
    explicit UserInterface(Board* board);

    /*
     * print sudoku board
     * */
    void render(std::string msg  = "");

    /*
     * asks for move and execute it
     * */
    void makeMove();

};


#endif //SUDOKU_USERINTERFACE_HPP
