//
// Created by radek on 10.12.2022.
//

#include <string>
#include <random>
#include <chrono>
#include "BoardFactory.hpp"
#include "Board.hpp"


unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
std::default_random_engine generator(seed);
std::uniform_int_distribution<int> distribution(1,9);

Board* BoardFactory::create() {
    board = new Board();
    fill();
    return board;
}

void BoardFactory::fill() {
    size_t box = 0;
    size_t numberOfPlaced = 0;
    size_t solutions = possibleSolutionsCount();
    while(numberOfPlaced < 17 || solutions != 1){
        std::pair<size_t, size_t> coords = getEmptyCoordsInBox(box%9%3,box%9/3);
        insertTile(coords.first,coords.second);
        numberOfPlaced++;
        solutions = possibleSolutionsCount();
        if(solutions == 0){
            board->removeTile(coords.first,coords.second);
        }else{
            box++;
        }
    }
    possibleSolutionsCount(0,0,0,false);
}


void BoardFactory::insertTile(size_t x, size_t y) {
    bool filled = false;
    while(!filled){
        size_t value = distribution(generator);
        if(!board->isProhibited(x,y,value)){
            board->setTile(x, y, value);
            filled = true;
        }
    }
}

size_t BoardFactory::possibleSolutionsCount(int x, int y, size_t count, bool del){
    if(x==0 && y == 9){
        count++;
        return count;
    }
    Tile* myTile = board->getTile(x, y);
    bool found = false;
    if(myTile == nullptr){
        for(int i = 1; i <= 9;i++){
            if(!board->isProhibited(x,y,i)){
                found = true;
                board->setTile(x,y,new Tile(i, false));
                int tmp = x+1 + y*9;
                count = possibleSolutionsCount(tmp % 9, tmp / 9, count,del);
            }
            if(!del && count == 1){
                return count;
            }
            if (count >= 2){
                board->removeTile(x,y);
                return count;
            }
        }
        if(found){
            board->removeTile(x,y);
        }
        return count;
    }
    else{
        int tmp = x+1 + y*9;
        return possibleSolutionsCount(tmp % 9, tmp / 9, count,del);
    }
}

std::pair<size_t, size_t> BoardFactory::getEmptyCoordsInBox(size_t boxX, size_t boxY) {
    size_t x;
    size_t y;
    do{
        x = boxX*3 + (distribution(generator)-1)/3;
        y = boxY*3 + (distribution(generator)-1)/3;
    }while(board->getTile(x, y) != nullptr);
    return std::make_pair(x,y);
}

