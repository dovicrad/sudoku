//
// Created by radek on 10.12.2022.
//

#ifndef SUDOKU_BOARD_HPP
#define SUDOKU_BOARD_HPP

#include <ctime>
#include <array>
#include "Tile.hpp"

class Board {
    std::array<std::array<Tile*, 9>, 9> matrix = {};

public:
    Board() = default;
    Tile* getTile(size_t x, size_t y);
    void setTile(size_t x, size_t y, Tile* tile);
    void setTile(size_t x, size_t y, size_t value);
    void removeTile(size_t x, size_t y);

    /*
     * returns true if value already exists in 3x3 box
     * */
    bool existsInBox(size_t boxX, size_t boxY, size_t value);

    /*
     * returns true if value already exists in row
     * */
    bool existsInRow(size_t y, size_t value);

    /*
     * returns true if value already exists in column
     * */
    bool existsInColumn(size_t x, size_t value);
    /*
     * returns true if value already exists box, row or column
     * */
    bool isProhibited(size_t x, size_t y, size_t value);

    /*
     * returns true if game is finished
     * */
    bool isCompleted();
};


#endif //SUDOKU_BOARD_HPP
