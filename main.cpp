#include <iostream>
#include "UserInterface.hpp"
#include "BoardFactory.hpp"


int main() {
    //setup terminal to accept ANSI color codes
    system( "chcp 65001" );

    auto* factory = new BoardFactory();
    Board* board = factory->create();
    auto* interface = new UserInterface(board);

    interface->render();
    while(!board->isCompleted()){
        interface->makeMove();
    }
    delete factory;
    delete board;
    delete interface;
    return 0;
}
