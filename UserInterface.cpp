//
// Created by radek on 10.12.2022.
//

#include "UserInterface.hpp"
#include <tuple>
#include <algorithm>
#include <thread>
#include <mutex>
#include <condition_variable>

UserInterface::UserInterface(Board* board) {
    this->board = board;
}

std::mutex currentLock;
size_t current;
size_t getCurrentRow() {
    std::lock_guard<std::mutex>lock(currentLock);
    return current++;
}

std::condition_variable threadRound;
std::mutex outputLock;
size_t currentRow;

std::string getRow(size_t row, Board *pBoard);

void printRow(std::string row, size_t rowNumber){
    std::unique_lock<std::mutex> lk(outputLock);
    threadRound.wait(lk, [rowNumber]{return rowNumber == currentRow;});
    std::cout << row;
    currentRow++;
    threadRound.notify_all();
}

auto characterThread = [](Board* board){
    size_t myRow = getCurrentRow();
    while(myRow < 20){
        printRow(getRow(myRow, board), myRow);
        myRow = getCurrentRow();
    }
};



void UserInterface::render(std::string msg) {
    printf("\x1B[2J\x1B[H");
    if(!msg.empty()){
        std::cout << msg << "\n";
    }

    currentRow = 0;
    current = 0;
    std::thread t1(characterThread,board);
    std::thread t2(characterThread,board);
    std::thread t3(characterThread,board);

    t1.join();
    t2.join();
    t3.join();

    std::cout << "\033[0m";
}

std::string getColor(size_t colorId) {
    switch(colorId){
        case 1:
            return "\x1b[31m";
        case 2:
            return "\x1b[35m";
        case 3:
        default:
            return "\x1b[37m";
    }
}

std::tuple<size_t, size_t, size_t> UserInterface::getMove() {
    std::string coordsString;
    std::string valueString;

    std::pair<size_t,size_t> coords;
    size_t value;

    bool valid = false;

    while(!valid){
        std::cout << "souradnice:";
        std::cin >> coordsString;
        if(checkForQ(coordsString)){
            return getMove();
        }
        coords = translateCoords(coordsString);
        if(coords.first > 0 && coords.first < 10 && coords.second > 0 && coords.second < 10 ){
            coords.first--;
            coords.second--;
            valid = true;
        }
    }
    valid = false;
    while(!valid){
        std::cout << "hodnota:";
        std::cin >> valueString;
        if(checkForQ(valueString)){
            return getMove();
        }
        value = translateValue(valueString);
        if(value > 0 && value < 10){
            valid = true;
        }
    }
    return std::make_tuple(coords.first, coords.second, value);
}

std::string getHeaderRow() {
    std::string finalRow;
    finalRow += getColor(1);
    finalRow += "  ";
    for(size_t column = 0; column < 9; column++){
        finalRow += "   ";
        finalRow += 49+column;
    }
    finalRow += (char)10;
    return finalRow;
}

std::string getNumberRow(size_t row, Board *pBoard) {
    std::string finalRow;
    size_t sepId = 0;
    finalRow += getColor(1);
    finalRow += letters[row];
    finalRow += " ";
    for(size_t column = 0; column < 9; column++){
        if(sepId %3 == 0){
            finalRow += getColor(1);
        }else{
            finalRow += getColor(3);
        }
        finalRow += " | ";
        sepId++;
        Tile* tile = pBoard->getTile(column, row);
        if(tile == nullptr || !tile->isVisible()){
            finalRow += " ";
        }else{
            finalRow += getColor(2);
            finalRow += std::to_string(tile->getValue());
        }
        finalRow += getColor(2);
    }
    finalRow += getColor(1);
    finalRow += " |";
    finalRow += (char)10;
    return  finalRow;
}

std::string getSeparatingRow(size_t row){
    std::string finalRow;
    finalRow += getColor(1);
    finalRow += "   +";
    for(size_t column = 0; column < 9; column++){
        if(row%3!=0){
            finalRow += getColor(3);
        }else{
            finalRow += getColor(1);
        }
        finalRow += "---";
        if(row%3!=0 && (column+1)%3!=0){
            finalRow += getColor(3);
        }else{
            finalRow += getColor(1);
        }
        finalRow += "+";
    }
    finalRow += (char)10;
    return finalRow;
}




std::pair<size_t, size_t> UserInterface::translateCoords(const std::string& coordsString) {
    std::pair<size_t, size_t> coords = std::make_pair(10, 10);
    for(char inputLetter : coordsString){
        for(size_t letter = 0; letter < 9; letter++){
            if(inputLetter == letters[letter]){
                coords.second = letter+1;
                break;
            }
        }
        if(std::isdigit(inputLetter)){
            coords.first = inputLetter - 48;
        }
    }
    return coords;
}

size_t UserInterface::translateValue(const std::string& valueString) {
    size_t value = 10;
    for(char inputLetter : valueString){
        if(std::isdigit(inputLetter)){
            value = inputLetter - 48;
        }
    }
    return value;
}

void UserInterface::makeMove() {
    std::tuple<size_t, size_t, size_t> move = getMove();
    Tile* target = board->getTile(std::get<0>(move), std::get<1>(move));
    if(target->getValue() == std::get<2>(move)) {
        target->setVisibility(true);
        if(board->isCompleted()){
            render("Hotovo");
            closeGame();
            return;
        }
    }
    else{
        render("Chyba!");
        return;
    }
    render();
}

bool UserInterface::checkForQ(const std::string& input) {
    for(char a : input){
        if(a == 'q'){
            return true;
        }
        if(a == 'Q'){
            closeGame();
        }
    }
    return false;
}

void UserInterface::closeGame() {
    exit(0);
}

std::string getRow(size_t row, Board *pBoard) {
    if(row == 0){
        return getHeaderRow();
    }
    if((row - 1) % 2 == 0){
        return getSeparatingRow((row - 1) / 2);
    }
    if((row - 1) % 2 == 1){
        return getNumberRow((row - 1) / 2, pBoard);
    }
    return {};
}
