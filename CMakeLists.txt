cmake_minimum_required(VERSION 3.5)
project(List LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(core-files
        BoardFactory.cpp
        BoardFactory.hpp
        UserInterface.cpp
        UserInterface.hpp
        Board.cpp
        Board.hpp
        Tile.cpp
        Tile.hpp)

add_executable(Sudoku main.cpp ${core-files})
add_executable(Test tests.cpp catch.hpp ${core-files})

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(Sudoku Threads::Threads)