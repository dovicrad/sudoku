//
// Created by radek on 10.12.2022.
//

#ifndef SUDOKU_TILE_HPP
#define SUDOKU_TILE_HPP


#include <ctime>
#include <iostream>

class Tile {
    size_t value;
    bool visible = true;
public:
    explicit Tile(size_t val, bool visibility = true);
    void setValue(size_t val);
    void setVisibility(bool);
    size_t getValue() const;
    bool isVisible() const;
};


#endif //SUDOKU_TILE_HPP
