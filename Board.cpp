//
// Created by radek on 10.12.2022.
//

#include "Board.hpp"

Tile *Board::getTile(size_t x, size_t y) {
    return matrix[x][y];
}

void Board::setTile(size_t x, size_t y, size_t value) {
    if(x < 0 || x > 8 || y < 0 || y > 8 || isProhibited(x,y,value)){
        throw std::range_error("cant place tile on the board");
    }
    else if(matrix[x][y] == nullptr){
        matrix[x][y] = new Tile(value);
    }
    else{
        matrix[x][y]->setValue(value);
    }
}

void Board::setTile(size_t x, size_t y, Tile *tile) {
    if(x < 0 || x > 8 || y < 0 || y > 8 || isProhibited(x,y,tile->getValue())){
        throw std::range_error("cant place tile on the board");
    }else{
        matrix[x][y] = tile;
    }
}

bool Board::existsInBox(size_t boxX, size_t boxY, size_t value) {
    for(size_t x = 3*boxX; x < 3*(boxX+1); ++x ){
        for(size_t y = 3*boxY; y < 3*(boxY+1); ++y ){
            if(matrix[x][y] != nullptr && matrix[x][y]->getValue() == value){
                return true;
            }
        }
    }
    return false;
}

bool Board::existsInRow(size_t y, size_t value) {
    for(auto & x : matrix){
        if(x[y] != nullptr && x[y]->getValue() == value){
            return true;
        }
    }
    return false;
}

bool Board::existsInColumn(size_t x, size_t value) {
    for(size_t y = 0; y < 9; y++){
        if(matrix[x][y] != nullptr && matrix[x][y]->getValue() == value){
            return true;
        }
    }
    return false;
}

bool Board::isProhibited(size_t x, size_t y, size_t value) {
    return existsInBox(x/3,y/3,value) || existsInColumn(x,value) || existsInRow(y,value) ;
}



void Board::removeTile(size_t x, size_t y) {
    if(matrix[x][y] != nullptr){
        delete matrix[x][y];
        matrix[x][y] = nullptr;
    }
}

bool Board::isCompleted() {
    for(auto& r : matrix){
        for(auto& c : r){
            if(c == nullptr || !c->isVisible()){
                return false;
            }
        }
    }
    return true;
}

