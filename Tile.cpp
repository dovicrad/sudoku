//
// Created by radek on 10.12.2022.
//

#include "Tile.hpp"

Tile::Tile(size_t val, bool visibility) {
    if(val >= 1 && val <= 9){
        this->value = val;
    }else{
        throw std::range_error("Tile value is not valid");
    }
    this->visible = visibility;
}

size_t Tile::getValue() const {
    return value;
}

void Tile::setValue(size_t val){
    if(val >= 1 && val <= 9){
        this->value = val;
    }else{
        throw std::range_error("Tile value is not valid");
    }
}

bool Tile::isVisible() const {
    return visible;
}

void Tile::setVisibility(bool visibility) {
    this->visible = visibility;
}
