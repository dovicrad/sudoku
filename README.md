# Sudoku

## Tato aplikace reprezentuje klasickou hru Sudoku
  - Spuští se bez parametrů
  - Hra se po spuštení zeptá na zadání souřadníce ukazujicí na pole kam chce uživatel zapsat hodnotu
    - příkad validního vstupu (a1,5i, 6b58da -> když je zadáno více hodnot, bere se pouze poslední, tedy 8a)
    - pro nový začátek zadávání souřadnic uživazel může zadat 'q' kamkoli do vstupu
    - pro ukončení programu uživazel může zadat 'Q' kamkoli do vstupu
  - Hrací pole se generuje automaticky
  - Testy pokrývají základní vlastnosti ovládání desky
  - Během vykreslování desky vlákna generují string, který reprezentuje jeden řádek desky. Ty poté postupně vytisknou
  - Dr. memmory po spuštění vypsal toto:

```
~~Dr.M~~ ERRORS FOUND:
~~Dr.M~~       0 unique,     0 total unaddressable access(es)
~~Dr.M~~       3 unique,    22 total uninitialized access(es)
~~Dr.M~~       0 unique,     0 total invalid heap argument(s)
~~Dr.M~~       0 unique,     0 total GDI usage error(s)
~~Dr.M~~       0 unique,     0 total handle leak(s)
~~Dr.M~~       0 unique,     0 total warning(s)
~~Dr.M~~       0 unique,     0 total,      0 byte(s) of leak(s)
~~Dr.M~~       0 unique,     0 total,      0 byte(s) of possible leak(s)
```