//
// Created by radek on 10.12.2022.
//

#ifndef SUDOKU_BOARDFACTORY_HPP
#define SUDOKU_BOARDFACTORY_HPP

#include "Board.hpp"
#include "UserInterface.hpp"

class BoardFactory {
    Board* board;
    void fill();
    void insertTile(size_t, size_t);

    /*
     * backtracking -> recursive algorithm to find sudoku solutions
     *
     * params:
     * x,y -> coordinates of tile
     * count -> number of found solutions
     * del -> determines if inserted tiles should be deleted
     *     -> can be used to solve a board
     *
     * return:
     * 0 -> no solutions
     * 1 -> one solution
     * 2 -> more than one solution
     * */
    size_t possibleSolutionsCount(int= 0, int= 0, size_t count = 0, bool del = true);

    /*
     * return: a pair of coordinates that contain no value in board
     * */
    std::pair<size_t,size_t> getEmptyCoordsInBox(size_t x, size_t y);
public:
    BoardFactory() = default;
    /*
     * creates and return new board
     * */
    Board* create();

};


#endif //SUDOKU_BOARDFACTORY_HPP
